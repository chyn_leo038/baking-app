package com.example.bakingapp;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.bakingapp.adapter.TabPagerAdapter;

import com.google.android.material.tabs.TabLayout;

public class HomeActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ViewPager tmpPager = findViewById(R.id.pager);
        TabPagerAdapter tmpPagerAdapter = new TabPagerAdapter(getSupportFragmentManager());
        tmpPager.setAdapter(tmpPagerAdapter);

        TabLayout tmpTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tmpTabLayout.setupWithViewPager(tmpPager);
    }
}
