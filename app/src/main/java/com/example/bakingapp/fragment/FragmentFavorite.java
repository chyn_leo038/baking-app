package com.example.bakingapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bakingapp.DetailRecipeActivity;
import com.example.bakingapp.R;
import com.example.bakingapp.adapter.RecipeAdapter;
import com.example.bakingapp.model.FavoriteViewModel;
import com.example.bakingapp.model.Recipe;

import java.util.ArrayList;
import java.util.List;

public class FragmentFavorite extends Fragment implements RecipeAdapter.ListClickListener{

    RecyclerView rvLayout;
    RecipeAdapter tmpAdapter = new RecipeAdapter(this);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View tmpView = inflater.inflate(R.layout.fragment_recipe,container,false);
        rvLayout = (RecyclerView) tmpView.findViewById(R.id.rv_Recipes);
        rvLayout.setHasFixedSize(true);
        rvLayout.setLayoutManager(new LinearLayoutManager(getActivity()));
        FavoriteViewModel viewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);
        viewModel.getFavorites().observe(this, new Observer<List<Recipe>>() {
                @Override
                public void onChanged(List<Recipe> recipes) {
                    Recipe[] recipes1 = new Recipe[recipes.size()];
                    for (int i=0; i<recipes.size();i++){
                        recipes1[i] = recipes.get(i);
                    }
                    tmpAdapter.setRecipes(recipes1);
                    rvLayout.setAdapter(tmpAdapter);
                }
            });


        return tmpView;
    }

    @Override
    public void onListClickListener(Recipe recipe) {
        Intent intent = new Intent(getActivity(), DetailRecipeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(DetailRecipeActivity.INTENT_DETAIL,recipe);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
