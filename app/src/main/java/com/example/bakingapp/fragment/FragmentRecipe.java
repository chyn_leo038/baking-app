package com.example.bakingapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bakingapp.DetailRecipeActivity;
import com.example.bakingapp.interfaces.IGetRecipeFromInternet;
import com.example.bakingapp.R;
import com.example.bakingapp.adapter.RecipeAdapter;
import com.example.bakingapp.model.Recipe;
import com.example.bakingapp.tasks.GetRecipeFromInternetTask;
import com.example.bakingapp.utilities.NetworkUtils;

import java.net.MalformedURLException;
import java.net.URL;

public class FragmentRecipe extends Fragment implements IGetRecipeFromInternet, RecipeAdapter.ListClickListener {

    RecyclerView rvLayout;
    RecipeAdapter tmpAdapter = new RecipeAdapter(this);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View tmpView = inflater.inflate(R.layout.fragment_recipe,container,false);
        rvLayout = (RecyclerView) tmpView.findViewById(R.id.rv_Recipes);
        rvLayout.setHasFixedSize(true);
        rvLayout.setLayoutManager(new LinearLayoutManager(getActivity()));

        URL url = null;
        try {
            url = NetworkUtils.buildUrl();
        } catch (MalformedURLException e){

        }
        if (url != null){
            new GetRecipeFromInternetTask(this).execute(url);
        }
        return tmpView;
    }

    @Override
    public void callback(Recipe[] recipe) {
        tmpAdapter = new RecipeAdapter(this);
        tmpAdapter.setRecipes(recipe);
        rvLayout.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvLayout.setAdapter(tmpAdapter);
    }

    @Override
    public void onListClickListener(Recipe recipe) {
        Intent intent = new Intent(getActivity(), DetailRecipeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(DetailRecipeActivity.INTENT_DETAIL,recipe);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
