package com.example.bakingapp.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.bakingapp.model.Recipe;

import java.util.List;

@Dao
public interface FavoriteDao {
    @Query("SELECT * FROM recipe")
    LiveData<List<Recipe>> loadAllFavorites();

    @Query("SELECT * FROM recipe WHERE id = :id")
    Recipe loadFavoriteById(int id);

    @Insert
    void insertFavorite(Recipe recipe);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateFavorite(Recipe recipe);

    @Delete
    void deleteFavorite(Recipe recipe);

}
