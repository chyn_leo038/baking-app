package com.example.bakingapp.database;

import androidx.room.TypeConverter;

import com.example.bakingapp.model.Ingredient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IngredientsConverter {
    @TypeConverter
    public static List<Ingredient> toList(String jsonIngredient){
        Gson tmpGSON = new GsonBuilder().create();
        Ingredient[] tmpIngredients = tmpGSON.fromJson(jsonIngredient, Ingredient[].class);
        return Arrays.asList(tmpIngredients);
    }
    @TypeConverter
    public static String toString(List<Ingredient> ingredients){
        Gson tmpGSON = new GsonBuilder().create();
        String tmpIngredients = tmpGSON.toJson(ingredients);
        return tmpIngredients;
    }

}
