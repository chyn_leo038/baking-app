package com.example.bakingapp.database;

import androidx.room.TypeConverter;

import com.example.bakingapp.model.Ingredient;
import com.example.bakingapp.model.Step;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StepsConverter {
    @TypeConverter
    public static List<Step> toList(String jsonStep){
        Gson tmpGSON = new GsonBuilder().create();
        Step[] tmpStep = tmpGSON.fromJson(jsonStep, Step[].class);
        return Arrays.asList(tmpStep);
    }
    @TypeConverter
    public static String toString(List<Step> steps){
        Gson tmpGSON = new GsonBuilder().create();
        String tmpStep = tmpGSON.toJson(steps);
        return tmpStep;
    }
}
