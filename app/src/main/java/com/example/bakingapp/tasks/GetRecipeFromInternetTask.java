package com.example.bakingapp.tasks;

import android.os.AsyncTask;

import com.example.bakingapp.interfaces.IGetRecipeFromInternet;
import com.example.bakingapp.model.Recipe;
import com.example.bakingapp.utilities.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.URL;

public class GetRecipeFromInternetTask extends AsyncTask<URL,Integer, Recipe[]>{

    private IGetRecipeFromInternet callBackHandler = null;
    public GetRecipeFromInternetTask(IGetRecipeFromInternet callback){
        this.callBackHandler = callback;
    }
    @Override
    protected Recipe[] doInBackground(URL... urls) {
        String tmpResult = "";
        try {
            tmpResult = NetworkUtils.getResponseFromHttpUrl(urls[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Gson tmpGSON = new GsonBuilder().create();
        Recipe[] tmpRecipes = tmpGSON.fromJson(tmpResult, Recipe[].class);

        return tmpRecipes;
    }

    @Override
    protected void onPostExecute(Recipe[] recipes) {
        super.onPostExecute(recipes);
        if(callBackHandler != null){
            callBackHandler.callback(recipes);
        }
    }
}
