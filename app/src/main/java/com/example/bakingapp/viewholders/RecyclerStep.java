package com.example.bakingapp.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bakingapp.R;
import com.example.bakingapp.adapter.StepAdapter;
import com.example.bakingapp.model.Step;

public class RecyclerStep extends RecyclerView.ViewHolder {

    private TextView tvStep;
    private ImageView btnPlay;

    public RecyclerStep(@NonNull View itemView) {
        super(itemView);
        tvStep = itemView.findViewById(R.id.tv_stepName);
        btnPlay = itemView.findViewById(R.id.btn_play);
    }

    public void bindData(final Step step, final StepAdapter.ListClickListener<Step> mOnClickListener){
        tvStep.setText(step.getShortDescription());
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickListener.onListClickListener(step);
            }
        });
    }
}
