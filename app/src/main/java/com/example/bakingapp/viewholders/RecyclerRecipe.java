package com.example.bakingapp.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bakingapp.R;
import com.example.bakingapp.adapter.RecipeAdapter;
import com.example.bakingapp.model.Recipe;

public class RecyclerRecipe extends RecyclerView.ViewHolder {

    private TextView tvRecipe;

    public RecyclerRecipe(@NonNull View itemView) {
        super(itemView);
        tvRecipe = itemView.findViewById(R.id.tv_Recipe);
    }

    public void bindData(final Recipe recipe, final RecipeAdapter.ListClickListener<Recipe> mOnClickListener){
        tvRecipe.setText(recipe.getName());
        tvRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickListener.onListClickListener(recipe);
            }
        });
    }
}
