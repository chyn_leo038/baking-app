package com.example.bakingapp;

import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bakingapp.adapter.StepAdapter;
import com.example.bakingapp.database.FavoriteDatabase;
import com.example.bakingapp.model.Ingredient;
import com.example.bakingapp.model.Recipe;
import com.example.bakingapp.model.Step;

import java.util.ArrayList;

public class DetailRecipeActivity extends AppCompatActivity implements StepAdapter.ListClickListener {

    private TextView tvRecipeName;
    private TextView tvNumbersofServing;
    private TextView tvListofIngredients;
    private ImageView ivFavorite;

    RecyclerView rvLayoutStep;
    StepAdapter tmpStepAdapter = new StepAdapter(this);

    private FavoriteDatabase mDb;

    private int DEFAULT_IMAGE_ID = R.drawable.iconnotfavorite;

    public static final String INTENT_DETAIL = "intent-detail";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_recipe);
        tvRecipeName = (TextView) findViewById(R.id.tv_recipe_name);
        tvNumbersofServing = (TextView) findViewById(R.id.tv_numbers_of_serving);
        tvListofIngredients = (TextView) findViewById(R.id.tv_list_of_ingredient);
        ivFavorite = (ImageView) findViewById(R.id.iv_favorite);

        mDb = FavoriteDatabase.getInstance(getApplicationContext());

        Intent intent = getIntent();
        if(intent.hasExtra(INTENT_DETAIL)){
            final Recipe recipe = (Recipe) intent.getParcelableExtra(INTENT_DETAIL);
            tvRecipeName.setText(recipe.getName());

            //favorite
            FavoriteExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    if(mDb.favoriteDao().loadFavoriteById(recipe.getId()) == null){
                        ivFavorite.setImageResource(R.drawable.iconnotfavorite);
                    } else {
                        ivFavorite.setImageResource(R.drawable.iconfavorite);
                        DEFAULT_IMAGE_ID = R.drawable.iconfavorite;
                    }
                }
            });

            ivFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(DEFAULT_IMAGE_ID == R.drawable.iconfavorite){
                        ivFavorite.setImageResource(R.drawable.iconnotfavorite);
                        DEFAULT_IMAGE_ID = R.drawable.iconnotfavorite;

                        FavoriteExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                mDb.favoriteDao().deleteFavorite(recipe);
                            }
                        });
                    } else if(DEFAULT_IMAGE_ID == R.drawable.iconnotfavorite){
                        ivFavorite.setImageResource(R.drawable.iconfavorite);
                        DEFAULT_IMAGE_ID = R.drawable.iconfavorite;

                        FavoriteExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                mDb.favoriteDao().insertFavorite(recipe);

                            }
                        });
                    }
                }
            });

            tvNumbersofServing.setText("Number of Servings : "+recipe.getServings());
            StringBuffer sbIngredients = new StringBuffer();
            for (Ingredient ingredient : recipe.getIngredients()){
                sbIngredients.append(" - ").append(ingredient.getIngredient()).append(" : ").append(ingredient.getQuantity()).append(" ").append(ingredient.getMeasure()).append("\n\n");
            }
            tvListofIngredients.setText(sbIngredients);

            //steps
            rvLayoutStep = (RecyclerView) findViewById(R.id.rv_Steps);

            rvLayoutStep.setHasFixedSize(true);
            rvLayoutStep.setLayoutManager(new LinearLayoutManager(this));
            tmpStepAdapter = new StepAdapter(this);
            tmpStepAdapter.setSteps((ArrayList<Step>) recipe.getSteps());
            rvLayoutStep.setAdapter(tmpStepAdapter);

        };
    }


    @Override
    public void onListClickListener(Step step) {
        Intent intent = new Intent(this, PopUpExoPlayer.class);
        intent.putExtra(PopUpExoPlayer.SHORT_DESCRIPTION_KEY,step.getShortDescription());
        intent.putExtra(PopUpExoPlayer.VIDEO_URL_KEY,step.getVideoURL());
        intent.putExtra(PopUpExoPlayer.DESCRIPTION_KEY,step.getDescription());
        startActivity(intent);
    }
}
