package com.example.bakingapp.interfaces;

import com.example.bakingapp.model.Recipe;


public interface IGetRecipeFromInternet {
    void callback(Recipe[] recipes);
}
