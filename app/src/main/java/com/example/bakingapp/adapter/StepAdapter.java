package com.example.bakingapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bakingapp.R;
import com.example.bakingapp.model.Step;
import com.example.bakingapp.viewholders.RecyclerRecipe;
import com.example.bakingapp.viewholders.RecyclerStep;

import java.util.ArrayList;

public class StepAdapter extends RecyclerView.Adapter {

    private final ListClickListener<Step> mOnClickListener;

    public StepAdapter(ListClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public interface ListClickListener<T>{
        void onListClickListener(Step step);
    }

    private ArrayList<Step> steps;

    public void setSteps(ArrayList<Step> steps){
        this.steps = steps;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.step_recycler, parent, false);
        RecyclerStep vh = new RecyclerStep(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((RecyclerStep) holder).bindData(steps.get(position), mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return steps.size();
    }
}
