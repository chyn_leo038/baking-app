package com.example.bakingapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bakingapp.R;
import com.example.bakingapp.model.Recipe;
import com.example.bakingapp.viewholders.RecyclerRecipe;

public class RecipeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ListClickListener<Recipe> mOnClickListener;

    public RecipeAdapter(ListClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public interface ListClickListener<T>{
        void onListClickListener(Recipe recipe);
    }

    private Recipe[] recipes;

    public void setRecipes(Recipe[] recipes){
        this.recipes = recipes;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_recycler, parent, false);
        RecyclerRecipe vh = new RecyclerRecipe(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((RecyclerRecipe) holder).bindData(recipes[position], mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return recipes.length;
    }
}
