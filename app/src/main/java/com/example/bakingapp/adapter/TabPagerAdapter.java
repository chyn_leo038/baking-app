package com.example.bakingapp.adapter;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.bakingapp.fragment.FragmentFavorite;
import com.example.bakingapp.fragment.FragmentRecipe;

public class TabPagerAdapter extends FragmentStatePagerAdapter {
    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("TEST", "Position ke - " + position);

        switch(position){
            case 0:
                return new FragmentRecipe();
            case 1 :
                return new FragmentFavorite();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return ("Recipe");
        } else if(position == 1){
            return ("Favorite");
        }
        return "";
    }
}
